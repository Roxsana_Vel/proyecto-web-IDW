<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>IDW</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">

</head>
<body>
   <div class="header">
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="#"><img src="img/logo1.fw.png" alt=""></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
        </ul>
        <form class="form-inline mt-2 mt-md-0">
            <ul class="navbar-nav  ">
                  <li class="nav-item active">
                    <a class="nav-link" href="#">NOSOTROS<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">SERVICIOS</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="#">PORTAFOLIO</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="#">BLOG</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="#">CONTACTO</a>
                  </li>
            </ul>
        </form>
      </div>
    </nav>

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="first-slide" src="img/2.jpg" alt="First slide">
          <div class="container">
            
          </div>
        </div>
        <div class="carousel-item">
          <img class="second-slide" src="img/2.jpg" alt="Second slide">
          <div class="container">
           
          </div>
        </div>
        <div class="carousel-item">
          <img class="third-slide" src="img/2.jpg" alt="Third slide">
          <div class="container">
            
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
       
   </div>
  <!-- Section: servicios -->
    <section  class="home-section text-center bg-gray">
		<div class="heading-about marginbot-50">
          <div class="container">
			    <div class="row">
			    <div class="col-lg-2"></div>
				<div class="col-lg-8 ">
					<div class="section-heading">
                        <h2>Nuestros Servicios</h2>
                        <hr class="subhr">
                        <p>Lorem ipsum dolor sit amet, no nisl mentitum recusabo per, vim at blandit qualisque dissentiunt. Diam efficiantur conclusionemque ut has</p>
					</div>
				</div>
			     </div>
			</div>
		</div>
    <div class="container">
        <div class="row">
           <div class="col-xs-6 col-sm-3 col-md-3">
            <div class="cardWrapper ">
                <div class="card">
                    <div class="cardFace front">
                       <div class="team boxed-white">
                          <div class="inner">
                              <div class="avatar"><img src="img/cloud.png" alt="" class="img-responsive" /></div>
                              <h5>DESAROLLO DE SOFWARE</h5>
                          </div>
                      </div>
                    </div>
                    <div class="cardFace back">
                       <div class="contenido-back">
                            <h5>Desarrollo de sofware</h5>
                            <p>Lorem ipsum dolor sit amet,consectetur adipisicing elit. Doloremque eligendi quisquam nam repellat placeat quam at a mollitia veritatis ad. A officiis veniam, quis commodi fugit, quam debitis quisquam quaerat.</p> 
                       </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3">
            <div class="cardWrapper">
                <div class="card">
                    <div class="cardFace front">
                       <div class="team boxed-white">
                          <div class="inner">
                              <div class="avatar"><img src="img/cloud.png" alt="" class="img-responsive" /></div>
                              <h5>DISEÑO WEB PROFESIONAL</h5>
                          </div>
                      </div>
                    </div>
                    <div class="cardFace back">
                        <div class="contenido-back">
                            <h5>Diseño Web Profesional</h5>
                            <p>Lorem ipsum dolor sit amet,consectetur adipisicing elit. Doloremque eligendi quisquam nam repellat placeat quam at a mollitia veritatis ad. A officiis veniam, quis commodi fugit, quam debitis quisquam quaerat.</p> 
                       </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3">
            <div class="cardWrapper ">
                <div class="card">
                    <div class="cardFace front">
                       <div class="team boxed-white">
                          <div class="inner">
                              <div class="avatar"><img src="img/innovate.png" alt="" class="img-responsive" /></div>
                              <h5>INNOVACIÓN EMPRESARIAL</h5>
                          </div>
                      </div>
                    </div>
                    <div class="cardFace back">
                        <div class="contenido-back">
                            <h5>Innovación Empresarial</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque eligendi quisquam nam repellat placeat quam at a mollitia veritatis ad. A officiis veniam, quis commodi fugit, quam debitis quisquam quaerat.</p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3">
            <div class="cardWrapper">
                <div class="card">
                    <div class="cardFace front">
                       <div class="team boxed-white">
                          <div class="inner">
                              <div class="avatar"><img src="img/hosting.png" alt="" class="img-responsive" /></div>
                              <h5> ALOJAMIENTO WEB </h5>
                          </div>
                      </div>
                    </div>
                    <div class="cardFace back">
                        <div class="contenido-back">
                            <h5>Alojamiento Web</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque eligendi quisquam nam repellat placeat quam at a mollitia veritatis ad. A officiis veniam, quis commodi fugit, quam debitis quisquam quaerat.</p>

                        </div>
                    
                    </div>
                </div>
            </div>

        </div>
        </div>
    </div>
</section>
  
   <section class="nosotros">
     <div class="container">
         <div class="row">
             <div class="col-lg-2"></div>
             <div class="col-lg-8">
                 <div class="box-naranja">
                     <hr class="style-about">
                     <p>Somos una empresa fundada en el año 2005 con experiencia en el desarrollo integral de soluciones empaquetadas y a medida usando tecnologías Internet, aplicaciones de negocio "web enabled", desarrollo web, marketing digital, identidad corporativa y manejo de campañas de Redes Sociales, brindando servicios de consultoría activa en tecnologías de la información, procesos de negocio y comunicación digital. </p><p>Además desde el 2015 brindamos asesoría a empresas y organizaciones en el desarrollo de innovación. Ayudamos en 3 etapas muy marcas: 1.- Concepción y desarrollo de propuestas innovadoras, 2.- Prototipado y validación de nuevos productos o servicios y 3.- Lanzamiento y crecimiento de.</p>
                 </div>
             </div>
         </div>
     </div>
   </section>
  
   <section class="portafolio">
       
       <div class="container">
         <hr class="style15">
           <div class="row">
                  <div class="col-xs-6 col-sm-4 col-md-4">
                    <div class="team boxed-grey">
                        <div class="inner">
                            <span class="fecha"><i></i>28 de Septiembre de 2017</span>
                            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis facere rerum sit debitis repellendus, autem, delectus aspernatur! Deleniti eaque laborum, dolorum voluptatem. Sed mollitia hic saepe inventore illum nihil, reiciendis.</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="autor">Mauro Rojas</p>
                                </div>
                                <div class="col-md-6">
                                    <buton class="btn">LEER MÁS</buton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4">
                       <div class="team boxed-grey">
                        <div class="inner">
                            <img src="img/calendar.png" width="15px;" alt=""><span class="fecha">28 de Septiembre de 2017</span>
                            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis facere rerum sit debitis repellendus, autem, delectus aspernatur! Deleniti eaque laborum, dolorum voluptatem. Sed mollitia hic saepe inventore illum nihil, reiciendis.</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="autor">Mauro Rojas</p>
                                </div>
                                <div class="col-md-6">
                                    <buton class="btn">LEER MÁS</buton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4">
                       <div class="team boxed-grey">
                        <div class="inner">
                            <span class="fecha"><i></i>28 de Septiembre de 2017</span>
                            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis facere rerum sit debitis repellendus, autem, delectus aspernatur! Deleniti eaque laborum, dolorum voluptatem. Sed mollitia hic saepe inventore illum nihil, reiciendis.</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="autor">Mauro Rojas</p>
                                </div>
                                <div class="col-md-6">
                                    <buton class="btn">LEER MÁS</buton>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
           </div>
       </div>
   </section>
   <section class="contacto">
         <div class="container">
              <div class="row">
                   <div class="col-md-4">
                       <div class="form-contacto">
                           <h3 >Contacto</h3>
                           <hr class="subhrc">
                           <p>Perez Gamboa <br>Gregorio Albaracin <br>Tacna-Perú <br>+51 952929939 <br> EMAIL MAP</p>
                       </div>
                   </div>

              </div>
        </div>
   </section>
   <secttion class="footer">
       <div class="container">
           <div class="row">
               <div class="col-md-3">
                   <img src="img/logo.png" alt="" width="200px;">
               </div>
               <div class="col-md-3">
                   <h3>CONTACTO</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores id ad tempora fugiat delectus.</p>
               </div>
               <div class="col-md-3">
                   <h3>QUE HACEMOS</h3>
               </div>
               <div class="col-md-3">
                    <h3>CONTACTANOS</h3>
               </div>
           </div>
       </div>
   </secttion>
   <section class="copy">
      <div class="container">
          
      </div>
       
   </section>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js"></script>
 <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
 <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
 <script src="js/main.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</html>